package org.eGovernance.panDtls.constant;

public class LookupConstant {

	public static final int PENDING_WITH_DA=5001;
	public static final int PENDING_WITH_AC=5002;
	public static final int APPROVED=5003;
	public static final int NO_ACTION=5004;
	
}
