package org.eGovernance.panDtls.service;


import org.eGovernance.panDtls.dao.CounterDao;
import org.eGovernance.panDtls.model.Counter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

import reactor.core.publisher.Mono;


@Service
public class BaseServiceImpl implements BaseService{

	MongoClient mongoClient=new MongoClient("localhost", 27017);
	@SuppressWarnings("deprecation")
	DB db=mongoClient.getDB("eGovernancePanDtls");
	
	@Override
	public Object getNextSequenceForPan(String name) throws Exception
	{
		DBCollection collection=db.getCollection("counter");
		BasicDBObject find=new BasicDBObject();
		
		find.put("_id", name);
		BasicDBObject update=new BasicDBObject();
		update.put("$inc", new BasicDBObject("seq",1));
		DBObject obj=collection.findAndModify(find, update);
		return obj.get("seq");
	}
	
	@Override
	public String getRandomStringChar(int length) {
		StringBuilder sb=new StringBuilder();
		int index=0;
		String alphaNumericString="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		
		for(int i=0;i<length;i++)
		{
			index=(int)(Math.random()*alphaNumericString.length());
			sb.append(alphaNumericString.charAt(index));
		}
		return sb.toString();
	}
	
	
	/*
	 * @Autowired private CounterDao counterDao;
	 * 
	 * @GetMapping("/save") public Mono<Counter> saveCounter() throws Exception {
	 * Counter c=new Counter(); c.setId("pan"); c.setSeq(0);
	 * counterDao.save(c).subscribe(); return null; }
	 */
}
