package org.eGovernance.panDtls.service;

public interface BaseService {
	
	public Object getNextSequenceForPan(String name) throws Exception;
	public String getRandomStringChar(int length);
	
}
