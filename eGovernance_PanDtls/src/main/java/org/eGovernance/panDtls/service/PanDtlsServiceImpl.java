package org.eGovernance.panDtls.service;

import org.eGovernance.panDtls.constant.LookupConstant;
import org.eGovernance.panDtls.dao.PanDtlsDao;
import org.eGovernance.panDtls.model.PanDtls;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class PanDtlsServiceImpl extends BaseServiceImpl implements PanDtlsService{

	private PanDtlsDao panDtlsDao;

	@Override
	public Mono<PanDtls> savePan(PanDtls panDtls) {
		try {
			panDtls.set_id(getNextSequenceForPan("pan"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		String appNo = "ARN"+String.format("%04d",panDtls.get_id());
		panDtls.setAppNo(appNo);
		panDtls.setStatus(LookupConstant.PENDING_WITH_DA);
		panDtlsDao.save(panDtls).subscribe();
		return Mono.just(panDtls);
	}
	
	@Override
	public Mono<PanDtls> findByUserName(String uName) {
		return panDtlsDao.findByUserName(uName);
	}
	
	@Override
	public Flux<PanDtls> findByStatus(int status) {
		return panDtlsDao.findByStatus(status);
	}
	
	@Override
	public Mono<PanDtls> processPan(String userName) {
		PanDtls pan=panDtlsDao.findByUserName(userName).block();
		pan.setStatus(LookupConstant.PENDING_WITH_AC);
		panDtlsDao.save(pan).subscribe();
		return Mono.just(pan);
	}

	@Override
	public Mono<PanDtls> approvePan(String userName) {
		PanDtls pan=panDtlsDao.findByUserName(userName).block();
		pan.setStatus(LookupConstant.APPROVED);
		
		StringBuilder sb=new StringBuilder(getRandomStringChar(5)); 
		 String str=null;
		
			str = String.format("%04d", pan.get_id());
		 sb.append(str).append(getRandomStringChar(1));
		 pan.setPanNo(sb.toString());
		panDtlsDao.save(pan).subscribe();
		return Mono.just(pan);
	}

	@Override
	public Mono<PanDtls> noAction(String userName) {
		PanDtls pan=panDtlsDao.findByUserName(userName).block();
		pan.setStatus(LookupConstant.NO_ACTION);
		panDtlsDao.save(pan).subscribe();
		return Mono.just(pan);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	@Autowired
	public PanDtlsServiceImpl(PanDtlsDao panDtlsDao) {
		this.panDtlsDao = panDtlsDao;
	}

	public PanDtlsServiceImpl() {
	}

	

	























	
























	

	
	
	
}
 