package org.eGovernance.panDtls.service;

import org.eGovernance.panDtls.model.PanDtls;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface PanDtlsService extends BaseService{

	Mono<PanDtls> savePan(PanDtls panDtls);

	Mono<PanDtls> findByUserName(String uName);

	Flux<PanDtls> findByStatus(int status);

	Mono<PanDtls> processPan(String userName);

	Mono<PanDtls> approvePan(String userName);

	Mono<PanDtls> noAction(String userName);

	
	
}
