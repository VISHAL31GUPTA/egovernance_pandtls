package org.eGovernance.panDtls.model;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Document
public @Data class Counter {

	private String id;
	private int seq;
	
}