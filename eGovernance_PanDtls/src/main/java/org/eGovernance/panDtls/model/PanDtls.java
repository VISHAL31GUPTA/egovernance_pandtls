package org.eGovernance.panDtls.model;

import java.time.LocalDate;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Document
public @Data class PanDtls {

	@Id 
	private Object _id;
	private String name;
	private String fatherName;
	private String panNo;
	private String appNo;
	private String email;
	private String userName;
	private int status;
	private LocalDate dob;
}
