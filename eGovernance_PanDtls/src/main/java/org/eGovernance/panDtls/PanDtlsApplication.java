package org.eGovernance.panDtls;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//@EnableEurekaClient
public class PanDtlsApplication {

	public static void main(String[] args) {
		SpringApplication.run(PanDtlsApplication .class, args);
	}

}

