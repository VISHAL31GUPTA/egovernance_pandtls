package org.eGovernance.panDtls.dao;

import org.eGovernance.panDtls.model.Counter;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CounterDao extends ReactiveMongoRepository<Counter, String>{

}
