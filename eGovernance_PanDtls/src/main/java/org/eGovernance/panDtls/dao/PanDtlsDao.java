package org.eGovernance.panDtls.dao;

import org.eGovernance.panDtls.model.PanDtls;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public interface PanDtlsDao extends ReactiveMongoRepository<PanDtls, Object>{
	
	public Mono<PanDtls> findByUserName(String userName);
	public Flux<PanDtls> findByStatus(int status);
	
}
