package org.eGovernance.panDtls.controller;

import org.eGovernance.panDtls.constant.LookupConstant;
import org.eGovernance.panDtls.model.PanDtls;
import org.eGovernance.panDtls.service.PanDtlsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/pan")
@CrossOrigin(origins = "http://localhost:4200")
public class PanDtlsController {

	private PanDtlsService panDtlsService;


	@PutMapping("/savePan")
	public Mono<PanDtls> savePan(@RequestBody PanDtls panDtls) throws Exception
	{	
		return panDtlsService.savePan(panDtls);
	}
	
	@GetMapping("/getPanByuName/{uName}")
	public Mono<PanDtls> findByUserName(@PathVariable(value = "uName") String uName) 
	{
		return panDtlsService.findByUserName(uName) ;
		
	}
	
	@GetMapping("/panByStatusDA") 
	public Flux<PanDtls> findByStatusDA() 
	{ 
		  return panDtlsService.findByStatus(LookupConstant.PENDING_WITH_DA); 
    }
	
	@GetMapping("/panByStatusAC") 
	public Flux<PanDtls> findByStatusAC() 
	{ 
		  return panDtlsService.findByStatus(LookupConstant.PENDING_WITH_AC); 
    }
	
	@PutMapping("/processPan")
	public Mono<PanDtls> processPan(@RequestBody String userName) 
	 {
		 return panDtlsService.processPan(userName);
	 }
	
	@PutMapping("/approvePan")
	public Mono<PanDtls> approvePan(@RequestBody String userName) 
	 {
		 return panDtlsService.approvePan(userName);
	 }
	
	@PutMapping("/noAction")
	public Mono<PanDtls> noAction(@RequestBody String userName) 
	 {
		 return panDtlsService.noAction(userName);
	 }
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	@Autowired
	public PanDtlsController(PanDtlsService panDtlsService) {
		this.panDtlsService = panDtlsService;
	}

	public PanDtlsController() {
	}
	
	
}
